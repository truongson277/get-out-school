module FormatDatetime
  def format_datetime_hour(datetime)
    datetime.present? ? datetime.strftime('%H:%M') : ''
  end

  def format_datetime_day(datetime)
    datetime.present? ? datetime.strftime('%d-%m-%Y') : ''
  end
end
