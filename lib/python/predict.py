# coding=utf8
import os, glob, json
from underthesea import chunk
from keras.models import load_model
import numpy as np
import sys


os.environ['TF_CPP_MIN_LOG_LEVEL'] ='2'

word_dic = { "_MAX":0 }
dic_file = "lib/python/data/review-dic.json"

# dic_file = "./data/review-dic.json"
global max_words


def main():
  global max_words
  global word_dic
  word_dic = json.load(open(dic_file))
  max_words = word_dic['_MAX']
  content =  sys.argv[1]
  words = data_chunk(content)
  f = open("lib/python/data/result.txt", "w")
  if (len(words) != 0):
    wt = "/".join(words)
    x = count_file_freq(wt)
    model = model_load()
    result = model.predict(np.array([x]))
    ranks = np.arange(1, 4).reshape(3, 1)
    predicted_rank = result.dot(ranks).flatten()
    rating = np.asscalar(predicted_rank)
    # f.write(str(rating))
    return str(rating)
  else:
    # f.write("0")
    return '0'

def model_load():
  filename = "lib/python/data/model_news.h5"
  # filename = "./data/model_news.h5"
  loaded_model = load_model(filename)
  # loaded_model = pickle.load(open(filename, 'rb'))
  return loaded_model


def data_chunk(text):
  _list = chunk(text)
  results_chunk = []
  for index, val in enumerate(_list):
      if val[1] not in ['V', 'R', 'A', 'X', 'C']: continue
      results_chunk.append(val[0])
  return results_chunk


def text_to_ids(text):
  global word_dic
  text = text.strip()
  text = text.replace('\n', '/')
  words = text.split("/")
  result = []
  for n in words:
    n = n.strip()
    if n == "": continue
    if not n in word_dic: continue
        # if word_dic["_MAX"] > cnt_max:continue
        # wid = word_dic[n] = word_dic["_MAX"]
        # word_dic["_MAX"] += 1
    else:
      wid = word_dic[n]
    result.append(wid)
  return result


def count_file_freq(wt):
  global max_words
  cnt = [0 for n in range(max_words)]
  text = wt.strip()
  ids = text_to_ids(text)
  for wid in ids:
    cnt[wid] += 1
  return cnt


if __name__ == '__main__':
  # main()
  json.dump(main(), sys.stdout)
