class SessionsController < ApplicationController
  skip_before_action :require_login, only: [:new, :create]

  def new
    if t("messages_alert.session.login").include?(params[:login]&.to_sym)
      @message = t("messages_alert.session.login.#{params[:login]}")
    end
    redirect_to root_path if logged_in?
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      login(user)
      redirect_to root_path(login: :success)
    else
      redirect_to login_path(login: :failed)
    end
  end

  def destroy
    log_out
    redirect_to root_path(logout: :success)
  end
end
