module Rating
  def calculate(rating)
    check = rating
    case check
    when 0 .. 0.5
      0
    when 0.5 .. 1.1
      1
    when 1.1 .. 1.5
      2
    when 1.99 .. 2.1
      5
    when 2.1 .. 2.2
      4
    else
      3
    end
  end
end
