class HomesController < ApplicationController
  include Rating
  require 'json'

  skip_before_action :require_login, only: [:index, :new, :create]

  def index
    if t("messages_alert.session.login").include?(params[:login]&.to_sym)
      @message = t("messages_alert.session.login.#{params[:login]}")
    elsif t("messages_alert.session.logout").include?(params[:logout]&.to_sym)
      @message = t("messages_alert.session.logout.#{params[:logout]}")
    end
  end

  def new
    @feedback = Feedback.new
    @cities = City.all
    @stores = Store.all
  end

  def create
    feedback = Feedback.new(feedback_params)
    respond_to do |format|
      format.js {
        if feedback.valid?
          return @messages = [t('messages_alert.feedback.create.max')] if feedback.content.length() > 250
          a = system("python3 lib/python/predict.py '#{feedback.content}'")
          rating_predict = File.read("lib/python/data/result.txt")
          rating = calculate(rating_predict.to_f)
          if rating != 0
            feedback.rating = rating
            feedback.save
            @messages = [t('messages_alert.feedback.create.success')]
            ThanksMailer.thanks(feedback.email, feedback.name).deliver_now
          else
            @messages = [t('messages_alert.feedback.create.spam')]
          end
        else
          @messages = [:name, :email, :content, :store_id].map do |param|
            unless feedback.errors.messages[param][0].blank?
              "#{ Feedback.human_attribute_name("#{param}")} #{feedback.errors.messages[param][0]}"
            else
              ""
            end
          end
        end
      }
    end
  end

  private
    def feedback_params
      params.require(:feedback).permit(:name, :email, :content, :store_id)
    end
end
