class Admin::StatisticsController < Admin::AdminController
  def index
    @feedback_data = Feedback.statistic
    @city_quantity = City.all.size
    @store_quantity = Store.all.size
  end
end
