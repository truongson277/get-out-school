class Admin::StoresController < Admin::AdminController

  def index
    respond_to do |format|
      format.json do
        render json: {
          stores: Store._city_id(params[:city_id])
        }.to_json
      end
      format.html do
        @stores = Store.includes(:city, :feedbacks)
      end
    end
  end

  def new
    @store = Store.new
    @cities = City.all
  end

  def create
    @store = Store.new(store_params.merge(user_id: @current_user.id))
    respond_to do |format|
      format.js {
        if @store.valid?
          @store.save
          @messages = [t('messages_alert.store.create.success')]
        else
          @messages = [:name, :address, :phone, :city_id, :status].map do |param|
            unless @store.errors.messages[param][0].blank?
              "#{ Store.human_attribute_name("#{param}")} #{@store.errors.messages[param][0]}"
            else
              ""
            end
          end
        end
      }
    end
  end

  def edit
    @store = Store.find_by(id: params[:id])
    @cities = City.all
  end

  def update
    respond_to do |format|
      format.js {
        @store = Store.find_by(id: params[:id])
        if @store.update(store_params)
          @messages = [t('messages_alert.store.update.success')]
        else
          @messages = [:name, :address, :phone, :city_id, :status].map do |param|
            unless @store.errors.messages[param][0].blank?
              "#{ Store.human_attribute_name("#{param}")} #{@store.errors.messages[param][0]}"
            else
              ""
            end
          end
        end
      }
    end
  end

   private
    def store_params
      params.require(:store).permit(:name, :address, :phone, :city_id, :status)
    end
end
