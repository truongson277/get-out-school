class Admin::CitiesController < Admin::AdminController

  skip_before_action :require_login, only: :stores

  def index
    @cities = City.all
  end

  def stores
    respond_to do |format|
      format.json do
        render json: {
          stores: Store._city_id(params[:city_id])
        }.to_json
      end
    end
  end

  def new
    @city = City.new
  end

  def create
    @city = City.new(city_params.merge(user_id: @current_user.id))
    respond_to do |format|
      format.js {
        if @city.valid?
          @city.save
          @messages = t('messages_alert.city.create.success')
        else
          @messages = "#{ City.human_attribute_name("name")} #{@city.errors.messages[:name][0]}"
        end
      }
    end
  end

  def edit
    @city = City.find_by(id: params[:id])
  end

  def update
    respond_to do |format|
      format.js {
        @city = City.find_by(id: params[:id])
        if @city.update(city_params)
          @messages = t('messages_alert.city.update.success')
        else
          @messages = t('messages_alert.city.update.failed')
        end
      }
    end
  end



  private
    def city_params
      params.require(:city).permit(:name)
    end
end
