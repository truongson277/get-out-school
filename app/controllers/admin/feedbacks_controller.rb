class Admin::FeedbacksController < Admin::AdminController
  def index
    @pagy, @feedbacks = pagy(Feedback.includes(:store).order(created_at: "desc"), items: 5)
  end
end
