// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require admin/echarts.min
//= require admin/areas.js
//= require admin/app
//= require admin/blockui.min
//= require admin/bootbox.min
//= require admin/bootstrap.bundle.min
//= require admin/bootstrap_multiselect
//= require admin/components_modals
//= require admin/d3.min
//= require admin/d3_tooltip
//= require admin/widgets_stats
//= require admin/dashboard
//= require admin/datatables.min
//= require admin/datatables_basic
//= require admin/daterangepicker
//= require admin/form_inputs
//= require admin/moment.min
//= require admin/select2.min
//= require admin/switchery.min
//= require admin/uniform.min
//= require admin/pnotify
//= require admin/interactions.min
//= require admin/fancybox.min
