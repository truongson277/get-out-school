var initHome = function() {
  var storeDisabled = function() {
    if( $('#city_id').val() === '') {
      $('#feedback_store_id').prop('disabled', true);
    }
  };

  var storeEnabled = function() {
    if( $('#city_id').val() !== '') {
      $('#feedback_store_id').prop('disabled', false);
    }
  };

  return {
    initAfterLoad: function() {
      storeDisabled();
    },

    storeEnabled: function() {
      storeEnabled();
    }
  }
}();
$(function(){
  initHome.initAfterLoad();
  var carouselRTL = false;

  if( $('body').hasClass('rtl') ) { carouselRTL = true; }

  $('#food-menu-carousel').owlCarousel({
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    items: 1,
    mouseDrag: false,
    dotsContainer: '#item-thumb',
    rtl: carouselRTL
  });

  $('#dessert-menu-carousel').owlCarousel({
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    items: 1,
    mouseDrag: false,
    dotsContainer: '#item-thumb1',
    rtl: carouselRTL
  });

  var stores = []

  $('#city_id').on('change', function() {
    var option_store = '<option value="">Chọn cửa hàng</option>';
    if ($(this).val() === '') {
      $('#feedback_store_id').prop('disabled', true);
      $('#feedback_store_id').empty();
      $("#feedback_store_id").append(option_store);
    } else {
      $.ajax({
        url: "/admin/cities/" + $(this).val() + "/stores",
        dataType: "json",
        type: "GET",
        success: function(data) {
          $('#feedback_store_id').prop('disabled', false);
          $('#feedback_store_id').empty();
          data['stores'].forEach(function(obj){
            option_store += "<option value=" + obj['id'] + ">" + obj['name'] + "</option>";
            stores[obj.id] = obj;
          })
          $("#feedback_store_id").append(option_store);
          // localStorage.setItem('recipes', JSON.stringify(recipes))
        }
      });
    }
  });

  $('#send_feedback').on('click', function () {
    $(this).text('ĐANG GỬI ...')
    $(this).prop('readonly', true)
  })

});
