class ThanksMailer < ApplicationMailer
  def thanks(email, name)
    @name = name
    mail to: email, subject: t('mail.feedback')
  end
end
