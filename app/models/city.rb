class City < ApplicationRecord
  has_many :stores
  belongs_to :user

  validates :name, presence: true
  validates :name, uniqueness: true
end
