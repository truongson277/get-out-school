class User < ApplicationRecord
  has_many :stores
  has_many :cities
  has_secure_password

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, uniqueness: true, presence: true, format: { with: VALID_EMAIL_REGEX }
  validates :name, :phone, :role, presence: true

  enum role: [:admin]
end
