class Feedback < ApplicationRecord
  belongs_to :store
  delegate :name, :address, to: :store, prefix: true

  validates :name, :email, :content, :store_id, presence: true

  scope :_day_between, -> (from_date, to_date) {
    where(created_at: from_date..to_date)
  }

  class << self
    def overview
      {
        quantity: Feedback.all.size,
        user: Feedback.group(:email).size.size
      }
    end

    def chart(from_day, to_day)
      {
        normal: Feedback._day_between(from_day, to_day).where("rating = ?", 3).size,
        good: Feedback._day_between(from_day, to_day).where("rating = ? OR rating = ?", 4, 5).size,
        bad: Feedback._day_between(from_day, to_day).where("rating = ? OR rating = ?", 1, 2).size
      }
    end

    def week_chart
      from_day = DateTime.now.beginning_of_week
      to_day = DateTime.now.at_end_of_week
      chart(from_day, to_day)
    end

    def month_chart
      from_day = DateTime.now.beginning_of_month
      to_day = DateTime.now.at_end_of_month
      chart(from_day, to_day)
    end

    def year_chart
      current_month = Time.now.month
      normals = Array.new(current_month, 0)
      goods = Array.new(current_month, 0)
      bads = Array.new(current_month, 0)
      months = (1..current_month).map do |month|
        "#{I18n.t('chart.month')} #{month}"
      end
      StatisticYear.all.each do |m|
        normals[m.month - 1] = m.normal
        goods[m.month - 1] = m.good
        bads[m.month - 1] = m.bad
      end

      {
        months:   months,
        normals:   normals,
        goods:     goods,
        bads:      bads
      }
    end

    def statistic
      {
        overview: overview,
        week_chart: week_chart,
        month_chart: month_chart,
        year_chart: year_chart
      }
    end
  end
end
