class Store < ApplicationRecord
  belongs_to :city
  belongs_to :user
  has_many   :feedbacks
  delegate :name, to: :city, prefix: true

  enum status: [:activity, :repair, :close]

  scope :_city_id, -> (city_id) { where(city_id: city_id) }

  validates :name, :address, :phone, :city_id, presence: true
  validates :address, uniqueness: true
end
