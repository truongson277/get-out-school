module Admin::StoresHelper
  def get_selection_status
    Settings.store_status.map do |r|
      [I18n.t("status.#{r}"), r.to_sym]
    end
  end

  def badge_class(text)
    check = text
    case check
    when 'close'
      'badge-danger'
    when 'repair'
      'badge-info'
    else
      'badge-success'
    end
  end
end
