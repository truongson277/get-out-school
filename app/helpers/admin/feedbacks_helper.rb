module Admin::FeedbacksHelper
  def get_stars(rating)
    stars = Array.new(5, "icon-star-empty3")
    (0...rating).each do |i|
      stars[i] = "icon-star-full2"
    end
    return stars
  end
end
