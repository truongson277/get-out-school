class StatisticBusinessByYear < ActiveRecord::Migration[5.2]
  def change
    sql =
    <<-SQL
      CREATE OR REPLACE VIEW statistic_years AS
        SELECT
          MONTH(DATE(CONVERT_TZ(feedbacks.created_at, '+00:00', '+07:00'))) AS month,
          (
            SELECT COUNT(id)
            FROM feedbacks
            WHERE
              rating = 3
              AND
                MONTH(DATE(CONVERT_TZ(feedbacks.created_at, '+00:00', '+07:00'))) = month
          ) AS normal,
          (
            SELECT COUNT(id)
            FROM feedbacks
            WHERE
              (rating = 4  OR rating = 5)
              AND
                MONTH(DATE(CONVERT_TZ(feedbacks.created_at, '+00:00', '+07:00'))) = month
          ) AS good,
          (
            SELECT COUNT(id)
            FROM feedbacks
            WHERE
              (rating = 1 OR rating = 2)
              AND
                MONTH(DATE(CONVERT_TZ(feedbacks.created_at, '+00:00', '+07:00'))) = month
          ) AS bad
        FROM feedbacks
        WHERE
          YEAR(DATE(CONVERT_TZ(feedbacks.created_at, '+00:00', '+07:00'))) = YEAR(now())
        GROUP BY month;
    SQL
    execute(sql)
  end
end
