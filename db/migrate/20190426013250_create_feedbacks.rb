class CreateFeedbacks < ActiveRecord::Migration[5.2]
  def change
    create_table :feedbacks do |t|
      t.references :store
      t.string :email
      t.string :name
      t.string :content
      t.integer :status
      t.integer :rating, default: 0

      t.timestamps
    end
  end
end
