class CreateStores < ActiveRecord::Migration[5.2]
  def change
    create_table :stores do |t|
      t.references :city
      t.references :user
      t.string :name
      t.string :address
      t.string :phone
      t.integer :status

      t.timestamps
    end
  end
end
